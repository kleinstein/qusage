
[TOC]

---

# QUSAGE: Quantitative Set Analysis for Gene Expression #

Enrichment analysis of gene sets is a popular approach that provides a functional interpretation of genome-wide expression data. Existing tests are affected by inter-gene correlations, resulting in a high Type I error. The most widely used test, Gene Set Enrichment Analysis, relies on computationally intensive permutations of sample labels to generate a null distribution that preserves gene-gene correlations. A more recent approach, CAMERA, attempts to correct for these correlations by estimating a variance inflation factor directly from the data. Although these methods generate P-values for detecting gene set activity, they are unable to produce confidence intervals or allow for post hoc comparisons. We have developed a new computational framework for Quantitative Set Analysis of Gene Expression (QuSAGE). QuSAGE accounts for inter-gene correlations, improves the estimation of the variance inflation factor and, rather than evaluating the deviation from a null hypothesis with a P-value, it quantifies gene-set activity with a complete probability density function. From this probability density function, P-values and confidence intervals can be extracted and post hoc analysis can be carried out while maintaining statistical traceability. Compared with Gene Set Enrichment Analysis and CAMERA, QuSAGE exhibits better sensitivity and specificity on real data profiling the response to interferon therapy (in chronic Hepatitis C virus patients) and Influenza A virus infection. QuSAGE is available as an R package, which includes the core functions for the method as well as functions to plot and visualize the results.

---

# QUSAGE Meta_Analysis #

## Motivation ##
Small sample sizes combined with high person-to-person variability can make it difficult to detect significant gene expression changes from transcriptional profiling studies. Subtle, but coordinated, gene expression changes may be detected using gene set analysis approaches. Meta-analysis is another approach to increase the power to detect biologically relevant changes by integrating information from multiple studies. Here, we present a framework that combines both approaches and allows for meta-analysis of gene sets. QuSAGE meta-analysis extends our previously published QuSAGE framework, which offers several advantages for gene set analysis, including fully accounting for gene-gene correlations and quantifying gene set activity as a full probability density function. 

## Results ##
Application of QuSAGE meta-analysis to influenza vaccination response shows it can detect significant activity that is not apparent in individual studies.

## Run QuSAGE meta-analysis and other meta-analyses

Please follow instructions below to run the case study in the manuscript.

1. Download [.zip file](https://bitbucket.org/kleinstein/qusage/src/tip/SupplementaryFile1.zip).

2. Unzip the .zip file to a directory where you will find following files:
	- **QuSAGE_data.rds**: contains data of a) BTM gene sets b) gene expression data and c)subject Ids of three studies. 
	- **Qusage_metaAnalysis.R**: R script to run the case study.
	- **README.pdf**: instructions to run case study.
	- **significantGeneSets.pdf**: an example output file of significant genes set with their p values and probability density functions (PDFs ).
	- **Alternative_metaAnalyses.R**: R script to run the case study using alternative meta-analysis methods, including:
		-  Fisher's method 
		-  Stouffer's method
		-  Directional Fisher's method
		-  Directional Stouffer's method
		-  Effect-size method

3. Install QuSAGE and required R packages:
	- source("https://bioconductor.org/biocLite.R")
	- biocLite("limma")
	- biocLite("qusage")
	- install.packages("RColorBrewer")
	- install.packages(“metaphor”)
	- install.packages(“metap”)


4. Open Qusage_metaAnalysis.R and set parameter "workDirectory" as current working directory where you can find QuSAGE_data.rds.

5. Run Qusage_metaAnalysis.R. If the script run successfully, it should generate a PDF file called significantGeneSets.pdf containing significant genes set with their p values and probability density functions (PDFs ).

6. Run Alternative_metaAnalyses.R. If the script run successfully, it should generate genes set with their p values and q values for five alternative meta-analysis methods.
## Contact ##

- [hailong.meng@yale.edu](mailto::hailong.meng@yale.edu?subject="QuSAGE meta-analysis")
- [steven.kleinstein@yale.edu](mailto::steven.kleinstein@yale.edu?subject="QuSAGE meta-analysis")
---


# Availability

The QuSAGE R package is available in Bioconductor and can be accessed from: http://bioconductor.org/packages/release/bioc/html/qusage.html.

